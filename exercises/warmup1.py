"""
Exercise warmup1
-----------------

Simple exercises to get used to python basics.

- From http://introtopython.org/var_string_num.html , do the following:
  - hello world
  - one variable, two messages
  - first name cases
  - full name
  - arithmetic

"""

# Write your solution here


- HELLO WORLD:
    
    In [3]: message="I like to learn to use python"

In [4]: print(message)
I like to learn to use python

- ONE VARIABLE, TWO MESSAGES:
    
    In [3]: message="I like to learn to use python"

In [4]: print(message)
I like to learn to use python

In [5]: message="Hola python"

In [6]: print(message)
Hola python

- FIRS NAME CASES:
    
    In [13]: first_name="jose"

In [14]: print (first_name)
jose

In [15]: print (first_name.lower())
jose

In [16]: print (first_name.title())
Jose

In [17]: print (first_name.upper())
JOSE

- FULL NAME:
    
    In [18]: first_name="Jose"

In [19]: print (first_name)
Jose

In [20]: last_name="Alcobendas"

In [21]: print (last_name)
Alcobendas

In [22]: print (first_name + last_name)
JoseAlcobendas

- ARITHMETIC:
    
    In [23]: print (10+10)
20

In [24]: print (10-9)
1

In [25]: print (2*5)
10

In [26]: print (20/2)
10.0

In [27]: print (5**2)
25